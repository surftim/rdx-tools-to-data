# surf-prototype

This repository contains the prototype plugins for Research Cloud in Surf.nl

## Requirements:

- Ansible 2.9 installed


In order to add or modify a catalog item please refer to this document.

https://atlas.ia.surfsara.nl/wiki/display/RSC/44+-+Catalog+Items
