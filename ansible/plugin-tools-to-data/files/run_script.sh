# script.py
# arg1: inputfolder
# arg2: outputfolder
# arg3: temp_folder
# arg4: log_folder

# write to local results folder first and copy folder after run?

# Close everything with UFW
sudo ufw default deny outgoing
sudo ufw default deny incoming
sudo ufw allow ssh
echo "y" | sudo ufw enable

cd /script

# sudo -u toolsuser strace -o /script/strace_output.txt  -e trace=network,openat  /script/tools_to_data/bin/python3 /script/script.py -i $1 -o $2 -t $3 1> stdout.txt 2> stderr.txt

sudo -u toolsuser /script/tools_to_data/bin/python3 /script/script.py -i $1 -o $2 -t $3 1> stdout.txt 2> stderr.txt

# open UFW out to http (for webdav)

sudo ufw allow out http
sudo ufw allow out https

# wait for webdav connection to re-establish
sleep 30

# cp /script/strace_output.txt $2/log
cp stdout.txt $4/log
cp stderr.txt $4/log

TIMESTAMP=$(date +%Y-%m-%d-%H%M)

echo  "$TIMESTAMP Finished" >> $4/log/report.txt

# wait for webdav connection to re-establish
sleep 30

command & disown
